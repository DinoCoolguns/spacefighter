#include "PowerupTest.h"

Powerup::Powerup()
{
	SetSpeed(200);
	SetCollisionRadius(10);
}

void Powerup::Update(const GameTime *pGameTime)
{
	if (p_cooldown != 0)
	{
		p_cooldown -= pGameTime->GetTimeElapsed();
	}
	else PowerupActive = false;

	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();

		GameObject::Update(pGameTime);
	}
}

void Powerup::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, 1);
	}
}

void Powerup::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;
}


void Powerup::PowerupGet()
{
	p_cooldown = 12;
	PowerupActive = true;
}