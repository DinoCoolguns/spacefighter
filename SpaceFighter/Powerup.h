#pragma once
#include "GameObject.h"

class Powerup : public GameObject
{
public:

	Powerup();
	virtual ~Powerup() { }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual std::string ToString() const { return "Powerup"; }

	virtual CollisionType GetCollisionType() const { return CollisionType::POWERUP; }

	virtual float GetSpeed() const { return m_speed; }

	virtual void SetSpeed(const float speed) { m_speed = speed; }

	virtual void Initialize(const Vector2 position, const double delaySeconds);

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual bool IsPowerupActive() { return m_isPowerupActive; }

	virtual void SetPowerupActive(bool powerupActive) { m_isPowerupActive = powerupActive; }

	virtual void SetCooldown(float activeTime) { m_cooldown = activeTime; }

	virtual float GetCooldown() { return m_cooldown; }

	void PowerupGet();

private: 

	bool m_isPowerupActive;
	float m_speed;
	float m_cooldown;
	float m_activationSeconds;
	double m_delaySeconds;
	Texture *m_pTexture;
};